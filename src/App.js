import './App.css'
import Main from './components/Main'
import Secret from './components/Secret'
import NotFound from './components/NotFound'
import CallBack from './components/CallBack'

function App(props) {
  let mainComponent = ""

  switch(props.location) {
    case "/":
      mainComponent = <Main {...props} />
      break
    case "/secret":
      mainComponent = props.auth.isAuthenticated() ? <Secret {...props} /> : <NotFound />
      break
    case "/callback":
        mainComponent = <CallBack />
        break
    default:
      mainComponent = <NotFound />
  }
  return (
    <div className="App">
      <header className="App-header">
        {mainComponent}
      </header>
    </div>
  );
}

export default App
