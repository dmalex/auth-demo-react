import React from "react"
//import {Component} from "react"

export default function NotFound() {
    return (
        <div>
            Not found!
            <br/>
            Jump to <a href="/">Home</a>
        </div>
    )
}

/*
export default class NotFound extends Component {
    render() {
        return (
            <div>
                Not found!
            </div>
        )
    }
}
*/
