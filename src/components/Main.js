import React from "react"
//import {Component} from "react"

export default function Main(props) {
    return (
        <div>
            <p className="Main">
                Hello, {props.name}<br/>
                To see the secret page <a href="/secret">click here</a>
            </p>
        
            {!props.auth.isAuthenticated() &&
            <div>
                <hr/>
                Please login first
                <hr/>
                <button onClick={props.auth.login} >Login</button>
            </div>
            }
        </div>
    )
}

/*
export default class Main extends Component {
    render() {
        return (
            <div>
                <p className="Main">
                    Hello, {this.props.name}<br/>
                    To see the secret page <a href="/secret">click here</a>
                </p>
                
                {!this.props.auth.isAuthenticated() &&
                <div>
                    <hr/>
                    Please login first
                    <hr/>
                    <button onClick={this.props.auth.login} >Login</button>
                </div>
                }
            </div>
        )
    }
}
*/
