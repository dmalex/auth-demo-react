import React from "react"
//import {Component} from "react"

export default function Secret(props) {
    return (
        <div>
            It's a secret page. Jump back to <a href="/">Home</a>
            <br/>
            <button onClick={props.auth.logout}>Logout</button>
        </div>
    )
}
  
/*
export default class Secret extends Component {
    render() {
        return (
            <div>
                It's a secret page. Jump back to <a href="/">Home</a>
                <br/>
                <button onClick={this.props.auth.logout}>Logout</button>
            </div>
        )
    }
}
*/
