import React, {useEffect, Component} from "react"
import Auth from "../Auth"
/*
function CallBack() {
  useEffect(() => {
    const auth = new Auth()
    auth.handleAuthentication()
  }, []);

  return (
    <div>
        Loading...
    </div>
  )
}

export default CallBack
*/

export default class Callback extends Component {
  componentDidMount() {
    const auth = new Auth()
    auth.handleAuthentication()
  }

  render() {
    return (
      <div>
        Loading...
      </div>
    )
  }
}
